"""
Código feito com auxílio do tutorial disponível no site https://www.learnopencv.com/deep-learning-based-human-pose-estimation-using-opencv-cpp-python/
e do código no repositório https://bitbucket.org/lucashnegri/pose_dardo/
"""

from cv2 import cv2
import config
import os
import shutil 

def analisePostura(pontos):
    """
    Antes de analisar, pegar apenas uma parte do vídeo
    Como fazer isso ? Não sei.
    Talvez não precise de ponto de outros atletas?
    Comparar cada membro e ver qual a posição de cada um
    Vários ifs 
    Retornar feedback

    Pontos iguais => mesma posição
    """

def mapearPontos(frame):
    # Guardar os arquivos treinados em variáveis
    protoFile = "pose/pose_deploy_linevec.prototxt"
    weigthsFile = "pose/pose_iter_440000.caffemodel"

    # Carregar o modelo treinado
    net = cv2.dnn.readNetFromCaffe(protoFile, weigthsFile)

    # Configurando tamanho da imagem
    inWidth = 368
    inHeight = 368

    # Preparando a imagem para alimentar a rede neural
    frameWidth, frameHeight = frame.shape[1], frame.shape[0]
    inpBlob = cv2.dnn.blobFromImage(frame, 1.0 / 255, (inWidth, inHeight), (0,0,0), swapRB=False, crop=False)

    # Alimentando a rede :D
    net.setInput(inpBlob)

    # Mapeamento dos pontos
    output = net.forward()

    H = output.shape[2]
    W = output.shape[3]

    # Pontos da foto
    points = []

    for i in range(18):
        # Mapa do corpo
        probMap = output[0, i, :, :]

        # Máxima global (?)
        minVal, prob, minLoc, point = cv2.minMaxLoc(probMap)

        # Ajuste da janela
        x = (frameWidth * point[0]) / W
        y = (frameHeight * point[1]) / H

        if prob > 0.1 :
            cv2.circle(frame, (int(x), int(y)), 3, (0, 255, 255), thickness=-1, lineType=cv2.FILLED)
            points.append((int(x), int(y)))
        else: 
            points.append(None)
    
    analisePostura(points)

    # Desenho do esqueleto
    for pair in config.POSE_PAIRS: 
        partA = pair[0]
        partB = pair[1]

        if points[partA] and points[partB]:
            cv2.line(frame, points[partA], points[partB], (0, 255, 0), 2)

    return frame

def separarVideo(arquivo):
    print("Preparando para extrair os frames...")
    extract = cv2.VideoCapture(arquivo)
    success, image = extract.read()
    count = 1
    images = []

    while success: 
        if count == 6: 
            print("Extraindo frame...")
            images.append(image)
            count = 1
        success, image = extract.read()
        count += 1

    return images

def juntarVideo():
    image_folder = 'img/'
    video_name = 'video/video.avi'

    #  Colocando todos os frames em um array    
    images = [img for img in os.listdir(image_folder)]
    frame = cv2.imread(os.path.join(image_folder, images[0]))
    height, width, layers = frame.shape

    # Criando vídeo
    video = cv2.VideoWriter(video_name, 0, 30, (width, height))

    # Juntando frames no vídeo
    for image in images: 
        video.write(cv2.imread(os.path.join(image_folder, image)))

def mapearFrames(arquivo):
    # Separando frames do vídeo para poder analisar a postura
    frames = separarVideo(arquivo)

    # Mapeando frames
    count = 1
    for frame in frames: 
        print("Mapeando frame " + str(count))
        frame = mapearPontos(frame)   
        count += 1      

    # Salvando numa pasta
    count = 1    
    for frame in frames: 
        cv2.imwrite(os.path.join("img/", "frame%d.jpg") %count, frame)
        count += 1
    
    juntarVideo()

def mapearImagem(arquivo):
    print("Executando, por favor aguarde...")
    arquivoMapeado = mapearPontos(arquivo)
    cv2.imwrite(os.path.join("img/", "frame.jpg"), arquivoMapeado)
    print("Olhe a sua pasta!")

def verificar(arquivo):
    if ".mp4" in arquivo:
        print("É um Video") 
        mapearFrames(arquivo)
    
    # pode bugar aqui, arrumar depois 
    else:
        print("É uma Imagem!")
        arquivo = cv2.imread(arquivo)

        mapearImagem(arquivo) 

if __name__ == "__main__":
    if not(os.path.isdir("img")): 
        os.mkdir("img")
    else:
        shutil.rmtree("img")
        os.mkdir("img")

    if not(os.path.isdir("video")):
        os.mkdir("video")

    # mudar nome do arquivo que deseja analisar a postura
    verificar("static/fig1.jpg")